**Ref-Card-03** is a **GitLab-repository** that allows me to do this task

Prerequisites:
- OS that supports Java
- Java

How to use:
```sh
git clone https://gitlab.com/arianfurrer/ref-card-03.git
```
